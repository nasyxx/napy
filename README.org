#+OPTIONS: ':nil *:t -:t ::t <:t H:4 \n:nil ^:{} arch:headline author:t
#+OPTIONS: broken-links:nil c:nil creator:nil d:(not "LOGBOOK") date:t e:t
#+OPTIONS: email:nil f:t inline:t num:nil p:nil pri:nil prop:nil stat:t tags:t
#+OPTIONS: tasks:t tex:t timestamp:t title:t toc:t todo:t |:t
#+TITLE: Napy
#+DATE: <2018-12-11 Tue>
#+AUTHOR: Nasy
#+EMAIL: nasyxx@gmail.com
#+LANGUAGE: en
#+SELECT_TAGS: export
#+EXCLUDE_TAGS: noexport
#+CREATOR: Emacs 27.0.50 (Org mode 9.1.9)

#+SETUPFILE: https://fniessen.github.io/org-html-themes/setup/theme-readtheorg.setup

Here is everything frequently use in python.

* Prologue

I often need to configure a new Python development environment.  Whether it is to help others or for
myself, it is very troublesome to manage packages with pip.  Besides, there are fascinating and
impressive ipython extensions, and every installation of them has to bother Google again.

Therefore, I created this napy.

/This package is still under development, and although is only for myself now, you can use it as you
like./

* Introduction

Napy includes some packages that I frequently use in python, such as ~requests~, for crawlers; ~sympy~
for mathematics.  Also, napy has some ipython extensions I write.  A template Napy also has that I
often use (of course, it's still straightforward now).  Hope you like it.

/Due to the ~.dir-local.el~ contains ~(org-html-klipsify-src . nil)~, it is warning that it is not safe./

* Usage

** Tools (Libs)

*** Utility

**** Flatten

Flatten list of iterable objects.

#+begin_src python
  from napy.tools import flatten, flatten_str

  list(flatten([1, 2, "ab", [3, "c", [4, ["d"]]]]))
  # [1, 2, "ab", 3, "c", 4, "d"]

  list(flatten("abc"))
  # ["a", "b", "c"]
  # regard "abc" as ["a", "b", "c"]

  list(flatten_str([1, 2, "ab", [3, "c", [4, ["d"]]]]))
  # or list(flatten([1, 2, "ab", [3, "c", [4, ["d"]]]], True))
  # [1, 2, "a", "b", 3, "c", 4, "d"]
#+end_src

** Comand Line Tools

*** Template

**** Crawler

#+begin_src shell
  $ napy template --help
  Usage:
    template [options]

  Options:
    -c, --category[=CATEGORY]       Category of template
    -o, --output[=OUTPUT]           Output file (default: "stdout")
    -y, --yes                       Confirmation
    -h, --help                      Display this help message
    -q, --quiet                     Do not output any message
    -V, --version                   Display this application version
        --ansi                      Force ANSI output
        --no-ansi                   Disable ANSI output
    -n, --no-interaction            Do not ask any interactive question
    -v|vv|vvv, --verbose[=VERBOSE]  Increase the verbosity of messages: 1 for normal output, 2 for more verbose output and 3 for debug

  Help:
   Template command line tool.
#+end_src

It will generate this:

#+begin_src python
  from requests_html import HtmlSession as s
  import requests as req


  def crawler() -> None:
      """Crawler."""
      pass


  if __name__ == "__main__":
      pass
#+end_src

** More

Still under development.

* Packages

** Normal

+ better_exceptions :: Pretty and helpful exceptions, automatically.
+ pendulum :: Python datetimes made easy.
+ tqdm :: Fast, Extensible Progress Meter.

** Science

+ jupyter :: Jupyter Notebook + IPython :: Jupyter metapackage. Install all the Jupyter components in
     one go.
+ numpy :: NumPy: array processing for numbers, strings, records, and objects
+ pandas :: Powerful data structures for data analysis, time series, and statistics
+ sympy :: Computer algebra system (CAS) in Python

** Crawler

+ requests :: Python HTTP for Humans.
+ requests_html :: HTML Parsing for Humans.
+ BeautifulSoup4 :: Screen-scraping library

** Development

+ cleo :: Cleo allows you to create beautiful and testable command-line interfaces.

* Epoligue

** History

#+include: "CHANGELOG" :minlevel 3
