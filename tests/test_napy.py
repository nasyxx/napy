#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Life's pathetic, have fun ("▔□▔)/hi~♡ Nasy.

Excited without bugs::

    |             *         *
    |                  .                .
    |           .
    |     *                      ,
    |                   .
    |
    |                               *
    |          |\___/|
    |          )    -(             .              ·
    |         =\ -   /=
    |           )===(       *
    |          /   - \
    |          |-    |
    |         /   -   \     0.|.0
    |  NASY___\__( (__/_____(\=/)__+1s____________
    |  ______|____) )______|______|______|______|_
    |  ___|______( (____|______|______|______|____
    |  ______|____\_|______|______|______|______|_
    |  ___|______|______|______|______|______|____
    |  ______|______|______|______|______|______|_
    |  ___|______|______|______|______|______|____

author   : Nasy https://nasy.moe
date     : Dec 11, 2018
email    : Nasy <nasyxx+python@gmail.com>
filename : test_napy.py
project  : napy.tests
license  : GPL-3.0+

There are more things in heaven and earth, Horatio, than are dreamt.
 --  From "Hamlet"
"""
# Standard Library
import subprocess
from tempfile import mkstemp

# Other Packages
from napy import tools, ipyext, console, template, __version__


def test_version() -> None:
    """Test version."""
    assert __version__ == "0.2.2"


def test_ipyext() -> None:
    """Test ipyext."""
    assert bool(ipyext.b_e.better_exceptions)
    assert bool(ipyext.ns.namespaces)


def test_template_crawler() -> None:
    """Test template."""
    assert template.to_str(template.crawler()) == (
        "from requests_html import HtmlSession as s\n"
        "import requests as req\n"
        "\n"
        "\n"
        "def crawler() -> None:\n"
        '    """Crawler."""\n'
        "    pass\n"
        "\n"
        "\n"
        'if __name__ == "__main__":\n'
        "    pass\n"
    )


def test_tools_utility_flatten() -> None:
    """Test tools utility flatten."""
    assert list(tools.flatten([1, 2, "ab", [3, "c", [4, ["d"]]]])) == [
        1,
        2,
        "ab",
        3,
        "c",
        4,
        "d",
    ]
    assert list(tools.flatten_str([1, 2, "ab", [3, "c", [4, ["d"]]]])) == [
        1,
        2,
        "a",
        "b",
        3,
        "c",
        4,
        "d",
    ]


def test_console() -> None:
    """Test console."""
    assert console


def test_console_template() -> None:
    """Test console template command."""
    assert subprocess.check_output(
        "poetry run napy template -c crawler -o <stdout> -y".split()
    ).decode() == (
        "---------\n"
        "from requests_html import HtmlSession as s\n"
        "import requests as req\n\n\n"
        "def crawler() -> None:\n"
        '    """Crawler."""\n'
        "    pass\n\n\n"
        'if __name__ == "__main__":\n'
        "    pass\n\n"
        "---------\n"
    )
    assert subprocess.check_output(
        "poetry run napy template -y".split(), input=b"\n\n"
    ).decode() == (
        "Select a category (default: crawler) [crawler]:\n"
        " [0] crawler\n"
        " > Output file (default: <stdout>): ---------\n"
        "from requests_html import HtmlSession as s\nimport requests as req"
        "\n\n\n"
        "def crawler() -> None:\n"
        '    """Crawler."""\n'
        "    pass\n\n\n"
        'if __name__ == "__main__":\n'
        "    pass\n\n"
        "---------\n"
    )


def test_console_template_with_file() -> None:
    """Test console template command with file."""
    file = mkstemp()
    subprocess.call(
        f"poetry run napy template -c crawler -o {file[1]} -y".split()
    )
    with open(file[1]) as f:
        assert f.read() == (
            "from requests_html import HtmlSession as s\n"
            "import requests as req\n\n\n"
            "def crawler() -> None:\n"
            '    """Crawler."""\n'
            "    pass\n\n\n"
            'if __name__ == "__main__":\n'
            "    pass\n"
        )
